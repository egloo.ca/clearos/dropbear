This is a dropbear build for ClearOS 5.  See the clearos5 branch for details.

You can download the ClearOS/CentOS 5 package from here:
http://mirrors.egloo.ca/egloo/clearos/5/devel/i386/

The usual daemon stuff is in place:
   - chkconfig dropbear on
   - service dropbear start

To avoid conflicts with the default OpenSSH server in ClearOS 5, the default port for Dropbear is set to 2222.  This can be changed in the /etc/sysconfig/dropbear configuration file.
